# docker + selenium

* https://github.com/SeleniumHQ/docker-selenium のPythonでの利用例。
* スクリプトは https://qiita.com/y_fujisawa/items/82bf616afbd8f5942c6a から拝借

## 動かし方

```
$ docker-compose up
```

でSeleniumのHubプロセスを動かす。

（しれっとPythonインタープリターが動いちゃってるのけど、気にしない）

```
$ docker-compose run --rm dev bash

# pip install selenium
# python main.py
```

で動作させる。

（playgroundのためだけにDockerfile作るのが面倒だったので、ベタにインストール＋実行させるようにした）
