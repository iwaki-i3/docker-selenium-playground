from selenium.webdriver import Remote
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class BasePage:
  def __init__(self, driver):
    self.driver = driver

  def find_element_by_id(self, id):
    return self.driver.find_element(By.ID, id)

  def find_element_by_xpath(self, xpath):
    return self.driver.find_element(By.XPATH, xpath)

class GoogleSearchTop(BasePage):
  def search(self, keyword):
    search_box = self.find_element_by_xpath('//*[@id="tsf"]/div[2]/div/div[1]/div/div[1]/input')
    search_box.send_keys(keyword + Keys.ENTER)  

class ResultPage(BasePage):
  def get_result_stats(self):
    result_stats = self.find_element_by_id('resultStats').text
    return result_stats

driver = Remote(command_executor='http://selenium-hub:4444/wd/hub',
                desired_capabilities=DesiredCapabilities.FIREFOX)
driver.implicitly_wait(10) # Firefoxではこれがないと検索ボタンクリック後にDOM取得に失敗する

# driver = Remote(command_executor='http://selenium-hub:4444/wd/hub',
#                 desired_capabilities=DesiredCapabilities.CHROME)

driver.get('http://www.google.com/xhtml')
GoogleSearchTop(driver).search("ブラウザ自動化ツール")
stats = ResultPage(driver).get_result_stats()
print(stats)
driver.quit()
